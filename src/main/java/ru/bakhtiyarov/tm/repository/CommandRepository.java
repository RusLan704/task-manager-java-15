package ru.bakhtiyarov.tm.repository;

import ru.bakhtiyarov.tm.api.repository.ICommandRepository;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.command.auth.LoginCommand;
import ru.bakhtiyarov.tm.command.project.*;
import ru.bakhtiyarov.tm.command.system.*;
import ru.bakhtiyarov.tm.command.task.*;
import ru.bakhtiyarov.tm.command.user.*;

import java.util.ArrayList;
import java.util.List;

public final class CommandRepository implements ICommandRepository {

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        commandList.add(new HelpCommand());
        commandList.add(new InfoCommand());
        commandList.add(new AboutCommand());
        commandList.add(new VersionCommand());
        commandList.add(new ShowCommand());
        commandList.add(new ArgumentsCommand());
        commandList.add(new LoginCommand());
        commandList.add(new ExitCommand());

        commandList.add(new TaskClearCommand());
        commandList.add(new TaskCreateCommand());
        commandList.add(new TaskListCommand());
        commandList.add(new TaskRemoveByIdCommand());
        commandList.add(new TaskRemoveByIndexCommand());
        commandList.add(new TaskRemoveByNameCommand());
        commandList.add(new TaskUpdateByIdCommand());
        commandList.add(new TaskUpdateByIndexCommand());
        commandList.add(new TaskViewByIdCommand());
        commandList.add(new TaskViewByIndexCommand());
        commandList.add(new TaskViewByNameCommand());

        commandList.add(new ProjectClearCommand());
        commandList.add(new ProjectClearCommand());
        commandList.add(new ProjectListCommand());
        commandList.add(new ProjectRemoveByIdCommand());
        commandList.add(new ProjectRemoveByIndexCommand());
        commandList.add(new ProjectRemoveByNameCommand());
        commandList.add(new ProjectViewByIdCommand());
        commandList.add(new ProjectViewByIndexCommand());
        commandList.add(new ProjectViewByNameCommand());
        commandList.add(new ProjectUpdateByIdCommand());
        commandList.add(new ProjectUpdateByIndexCommand());

        commandList.add(new UserViewProfileCommand());
        commandList.add(new UserUpdateLoginCommand());
        commandList.add(new UserUpdatePasswordCommand());
        commandList.add(new UserUpdateFirstNameCommand());
        commandList.add(new UserUpdateMiddleNameCommand());
        commandList.add(new UserUpdateLastNameCommand());
        commandList.add(new UserUpdateEmailCommand());
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}