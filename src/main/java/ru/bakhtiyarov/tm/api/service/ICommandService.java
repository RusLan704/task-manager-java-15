package ru.bakhtiyarov.tm.api.service;

import ru.bakhtiyarov.tm.command.AbstractCommand;
import java.util.List;

public interface ICommandService {

   List<AbstractCommand> getCommandList();

}
