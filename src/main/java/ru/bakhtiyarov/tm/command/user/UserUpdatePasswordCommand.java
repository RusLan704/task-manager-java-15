package ru.bakhtiyarov.tm.command.user;

import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.USER_UPDATE_PASSWORD;
    }

    @Override
    public String description() {
        return "Update user password.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PASSWORD]");
        final String userId = serviceLocator.getAuthService().getUserId();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        User userUpdated = serviceLocator.getUserService().updatePassword(userId, password);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}