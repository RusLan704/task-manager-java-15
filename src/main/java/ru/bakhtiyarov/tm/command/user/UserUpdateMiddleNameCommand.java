package ru.bakhtiyarov.tm.command.user;

import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.constant.TerminalConst;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public class UserUpdateMiddleNameCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.USER_UPDATE_MIDDLE_NAME;
    }

    @Override
    public String description() {
        return "Update user middle name.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE MIDDLE NAME]");
        final String userId = serviceLocator.getAuthService().getUserId();
        final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        final User userUpdated = serviceLocator.getUserService().updateUserMiddleName(userId, middleName);
        if (userUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}