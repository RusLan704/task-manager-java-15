package ru.bakhtiyarov.tm.exception;

public class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(String message) {
        super(message);
    }

}