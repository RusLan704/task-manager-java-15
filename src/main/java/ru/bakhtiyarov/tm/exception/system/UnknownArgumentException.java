package ru.bakhtiyarov.tm.exception.system;

import ru.bakhtiyarov.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(final String arg) {
        super("Error! Command ``" + arg + "`` doesn't exist...");
    }

}